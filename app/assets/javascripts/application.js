// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs



//= require respond.min.js
//= require jquery-1.11.0.min.js
//= require bootstrap.min.js
//= require jquery.cookie.js
//= require waypoints.min.js
//= require modernizr.js
//= require bootstrap-hover-dropdown.js
//= require owl.carousel.min.js
//= require front.js



